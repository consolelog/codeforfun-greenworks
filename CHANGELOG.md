# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.3.0](https://gitee.com/consolelog/codeforfun-greenworks/compare/v1.2.0...v1.3.0) (2023-03-14)


### Features

* 添加文档及readme.md ([05001be](https://gitee.com/consolelog/codeforfun-greenworks/commit/05001be75ea95299f4b7df2b6a1d279c9556119f))
* 添加asciidoctor.js ([66e4e4b](https://gitee.com/consolelog/codeforfun-greenworks/commit/66e4e4bb78b69deae131df425d5908356dedb756))


### Bug Fixes

* 生成html格式错误 ([7906324](https://gitee.com/consolelog/codeforfun-greenworks/commit/7906324c9944cd6799730bda9685c8d4213270ac))

## [1.2.0](https://gitee.com/consolelog/codeforfun-greenworks/compare/v1.1.0...v1.2.0) (2023-03-12)


### Features

* 添加ugcGetItemState方法返回值 ([fdac098](https://gitee.com/consolelog/codeforfun-greenworks/commit/fdac098f3e2e0c86f6361def11e7a06e95d664db))

## 1.1.0 (2023-03-11)


### Features

* 调试greenworks 3c3341e
